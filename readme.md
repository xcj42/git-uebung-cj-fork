# git cheat sheet

Read your group instruction in the text files 

[group1.md](group1.md)

[group2.md](group2.md)

[group3.md](group3.md)

[group4.md](group4.md)

and place your solution in a fitting category in this file. It's best to create a branch while working on the exercise!

## Creating repositories

... put commands and explanation here

## Staging and committing

... put commands and explanation here

## Inspecting the repository and history

... put commands and explanation here

## Managing branches

    $ git checkout master 		legt den Pointer auf den master Branch
    $ git branch feature1 		erzeugt neuen Branch "feature1"
    $ git checkout -b work 		erzeugt neuen Branch "work" und legt den Pointer auf diesen Branch 
    $ git checkout work 		legt Pointer auf den Branch "work"
    $ git branch 				listet die lokalen Branches auf
    $ git branch -v 			listet die lokalen Branches und den letzten Commit auf
    $ git branch -a 			listet alle (remote und lokale) Branches auf
    $ git branch --merged 		zeigt alle Branches welche mit dem aktuellen gemerged sind
    $ git branch --no-merged 	zeigt alle Branches, welche mit dem aktuellen NICHT gemerged sind
    $ git branch -d work 		löscht lokal Branch "work"
    $ git branch -D work 		löscht lokal Branch "work" und ignoriert dabei Konflikte

## Merging

.. put commands and examples here